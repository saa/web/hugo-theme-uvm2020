# hugo-theme-uvm2020

## Features

## Installation

1. Add the following to your `hugo.toml` site configuration file:

``` toml
[[module.imports]]
    path = "gitlab.uvm.edu/saa/web/hugo-theme-uvm2020"
```

2. Run `hugo mod get` to install or update modules.

## Configuration
